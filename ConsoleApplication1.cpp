﻿
#include <iostream>
using namespace std;

struct TNode {
    int Data;
    TNode* pNext;
};
void create(TNode** pBegin, TNode** pEnd, int Data) {
    *pBegin = new TNode;
    (*pBegin)->Data = Data;
    (*pBegin)->pNext = NULL;
    *pEnd = *pBegin;
}

void add(TNode** pEnd, int Data) {
    TNode* pv = new TNode;
    pv->Data = Data;
    pv->pNext = NULL;
    (*pEnd)->pNext = pv;
    *pEnd = pv;
}

int del(TNode** pBegin, TNode** pEnd, int Data) {
    TNode* pPrev = NULL;
    TNode* pCurrent =  * pBegin;
    *pBegin = (*pBegin)->pNext;
    int value = pCurrent->Data;
    delete pCurrent;
    return value;
}

int main()
{
    TNode* pBegin = NULL;
    TNode* pEnd = NULL;
    int Data = 0, Data2 = 0, Data3=0,Data4=0, Car = 0, Car2 = 0, Car3 = 0, Car4 = 0;
    cout << "Skolko mashin na kolonke 92 ";
    cin >> Car;
    Data = Car;
    cout << "Skolko mashin na kolonke 95 ";
    cin >> Car2;
    Data2 = Car2;
    cout << "Skolko mashin na kolonke 98 ";
    cin >> Car3;
    Data3 = Car3;
    cout << "Skolko mashin na kolonke Dizel ";
    cin >> Car4;
    Data4 = Car4;
    create(&pBegin, &pEnd, Car);
    for (int i = 1; i < Car; i++)
        add(&pEnd, Car);
    for (int i = 0; i < Car; i++) {

        cout << "\nMashin ostalos na 92 kolonke ";
        del(&pBegin, &pEnd, Car);
        Data = Data -1;
        cout << Data;
    }
    create(&pBegin, &pEnd, Car2);
    for (int i = 1; i < Car2; i++)
        add(&pEnd, Car2);
    for (int i = 0; i < Car2; i++) {

        cout << "\nMashin ostalos na 95 kolonke ";
        del(&pBegin, &pEnd, Car2);
        Data2 = Data2 - 1;
        cout << Data2;
    }
    create(&pBegin, &pEnd, Car3);
    for (int i = 1; i < Car3; i++)
        add(&pEnd, Car3);
    for (int i = 0; i < Car3; i++) {

        cout << "\nMashin ostalos na 98 kolonke ";
        del(&pBegin, &pEnd, Car3);
        Data3 = Data3 - 1;
        cout << Data3;
    }
    create(&pBegin, &pEnd, Car4);
    for (int i = 1; i < Car4; i++)
        add(&pEnd, Car4);
    for (int i = 0; i < Car4; i++) {

        cout << "\nMashin ostalos na Dizel kolonke ";
        del(&pBegin, &pEnd, Car4);
        Data4 = Data4 - 1;
        cout << Data4;
    }
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
